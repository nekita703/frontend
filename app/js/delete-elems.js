export default class deleteElems {
    item = '[kill-cart]';

    constructor() {
        console.log($(this.reverse));
        this.events();
    }


    killCart($this){
        let quad = $this.closest($('#parts_quad'));
        quad[0].remove();
    }

    events() {
        let self = this;

        $(document).on('click', '[kill-cart]', function(e) {
            // e.preventDefault();
            self.killCart($(this));

        });
    }
}

