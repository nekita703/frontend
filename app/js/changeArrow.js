export default class changeArrow {
    reverse = '[data-reverse]';
    table = '[data-table]';
    pr = '[data-pr]';
    row = '[data-row]';

    constructor() {
        // console.log($(this.reverse));
        this.events();
    }


    changeArrow($this) {
        let arrow = $this.closest($('[data-change-arrow]'));

        if (arrow[0].childNodes[1].childNodes[1].classList[2] == "rotate") {
            arrow[0].childNodes[1].childNodes[1].classList.remove('rotate')
        } else {
            arrow[0].childNodes[1].childNodes[1].classList.add('rotate');
        }
    }

    events() {
        let self = this;

        $(document).on('change', '[data-change-arrow]', function(e) {
            e.preventDefault();
            self.changeArrow($(this));
        });
    }
}

