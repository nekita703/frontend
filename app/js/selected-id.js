export default class SelectedId {

    constructor() {
        // console.log($(this.reverse));
        this.events();
    }

    selectedLvl($this) {
        let id = '';
        let block = 'block-';
        let selectedId = $this.closest($('[data-selected-id]'));
        // console.log(selectedId[0].id);
        for (let i=6; i<selectedId[0].id.length; i++){
            id = id + selectedId[0].id[i];
            console.log(selectedId[0].id[i]);
        }
        block = block + id;
        // console.log('block =', block);
        let selectedBlock = document.getElementById(block);
        selectedBlock.classList.remove('opacity-0');
        // console.log(selectedBlock);
    }

    unselectedLvl($this) {
        let id = '';
        let block = 'block-';
        let selectedId = $this.closest($('[data-selected-id]'));
        // console.log(selectedId[0].id);
        for (let i=6; i<selectedId[0].id.length; i++){
            id = id + selectedId[0].id[i];
            // console.log(selectedId[0].id[i]);
        }
        block = block + id;
        // console.log('block =', block);
        let selectedBlock = document.getElementById(block);
        selectedBlock.classList.add('opacity-0');
        // console.log(selectedBlock);
    }

    selectedBlock($this) {
        let id = '';
        let block = 'level-';
        let selectedId = $this.closest($('[block-selected-id]'));
        // console.log(selectedId[0].id);
        for (let i=6; i<selectedId[0].id.length; i++){
            id = id + selectedId[0].id[i];
            // console.log(selectedId[0].id[i]);
        }
        block = block + id;
        // console.log('level =', block);
        let selectedBlock = document.getElementById(block);
        selectedBlock.classList.add('orange');
        selectedId[0].classList.remove('opacity-0')
        selectedId[0].classList.add('opacity-100');
        // console.log(selectedBlock);
    }

    unselectedBlock($this) {
        let id = '';
        let block = 'level-';
        let selectedId = $this.closest($('[block-selected-id]'));
        // console.log(selectedId[0].id);
        for (let i=6; i<selectedId[0].id.length; i++){
            id = id + selectedId[0].id[i];
            // console.log(selectedId[0].id[i]);
        }
        block = block + id;
        // console.log('level =', block);
        let selectedBlock = document.getElementById(block);
        selectedBlock.classList.remove('orange');
        selectedId[0].classList.add('opacity-0')
        selectedId[0].classList.remove('opacity-100');
        // console.log(selectedBlock);
    }

    events() {
        let self = this;

        $(document).on('mouseenter', '[data-selected-id]', function (e) {
            e.preventDefault();
            self.selectedLvl($(this));
        });

        $(document).on('mouseleave', '[data-selected-id]', function (e) {
            e.preventDefault();
            self.unselectedLvl($(this));
        });

        $(document).on('mouseenter', '[block-selected-id]', function (e) {
            e.preventDefault();
            self.selectedBlock($(this));
        });

        $(document).on('mouseleave', '[block-selected-id]', function (e) {
            e.preventDefault();
            self.unselectedBlock($(this));
        });
    }
}

