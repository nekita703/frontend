import { Swiper, Navigation, Pagination } from 'swiper/dist/js/swiper.esm.js'
import 'swiper/dist/css/swiper.min.css'

Swiper.use([Navigation, Pagination]);

export default class Sliders {
    constructor() {
        let self = this;

        this.sliderHit = [
            {
                'selector': '#sliderHit .swiper-container',
                'options': {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    slidesPerGroup: 4,
                    speed: 700,
                    loop: true,
                    pagination: {
                        el: '#sliderHit .swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '#sliderHit .swiper-button-next',
                        prevEl: '#sliderHit .swiper-button-prev',
                    },
                    breakpoints: {
                        800: {
                            slidesPerView: 2,
                            slidesPerGroup: 2,
                            spaceBetween: 15
                        },
                        1200: {
                            slidesPerView: 3,
                            slidesPerGroup: 3,
                            spaceBetween: 20
                        },
                    }
                }
            }
        ];


        this.sliderNews = [
            {
                'selector': '#sliderNews .swiper-container',
                'paginator': '#sliderNews .two',
                'options': {
                    slidesPerView: 2,
                    spaceBetween: -10,
                    slidesPerGroup: 1,
                    speed: 700,
                    loop: true,
                    pagination: {
                        el: '#sliderNews .swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '#sliderNews .swiper-button-nextt',
                        prevEl: '#sliderNews .swiper-button-prevv',
                    },
                    breakpoints: {

                    }
                }
            }
        ];


        this.sliderBuy = [
            {
                'selector': '#sliderBuy .swiper-container',
                'options': {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    slidesPerGroup: 1,
                    speed: 700,
                    loop: true,
                    pagination: {
                        el: '#sliderBuy .swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '#sliderBuy .swiper-button-nexttt',
                        prevEl: '#sliderBuy .swiper-button-prevvv',
                    },
                    breakpoints: {
                    }
                }
            }
        ];

        this.sliderBuyMobile = [
            {
                'selector': '#sliderBuyMobile .swiper-container',
                'options': {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    slidesPerGroup: 1,
                    speed: 700,
                    loop: true,
                    pagination: {
                        el: '#sliderBuyMobile .swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '#sliderBuyMobile .swiper-button-nexttt',
                        prevEl: '#sliderBuyMobile .swiper-button-prevvv',
                    },
                    breakpoints: {
                    }
                }
            }
        ];

        // this.galleryThumbs = [
        //     {
        //         'selector': '.gallery-thumbs',
        //         'options' :{
        //             spaceBetween: 0,
        //             slidesPerView: 5,
        //             loop: true,
        //             freeMode: true,
        //             loopedSlides: 5, //looped slides should be the same
        //             // navigation: {
        //             //     nextEl: '.swiper-button-next',
        //             //     prevEl: '.swiper-button-prev',
        //             // },
        //             watchSlidesVisibility: false,
        //             watchSlidesProgress: false,
        //     }
        //     }
        //  ];

        // this.galleryTop = [
        //     {
        //         'selector': '.gallery-top',
        //         'options': {
        //             spaceBetween: 0,
        //             slidesPerView: 1,
        //             loop: true,
        //             loopedSlides: 5, //looped slides should be the same
        //             navigation: {
        //                 nextEl: '.swiper-button-next',
        //                 prevEl: '.swiper-button-prev',
        //             },
        //             thumbs: {
        //                 swiper: this.galleryThumbs,
        //             },
        //         }
        //     }
        //     ];


        this.init();
        this.events();
    }

    init() {
        // this.sliderHit.forEach(function (slider) {
        //     if ($(slider.selector).find($('.swiper-slide')).length > 4) {
        //         new Slider(slider.selector, slider.options)
        //     } else {
        //         $(slider.selector).closest('.slider-catalog').find('.slider-catalog__head').addClass('slider-catalog__head--nav-hide');
        //     }
        // });
        this.sliderNews.forEach(function (slider) {
            if ($(slider.selector).find($('.swiper-slide')).length > 2) {
                new Slider(slider.selector, slider.options)
            } else {
                $(slider.selector).closest('.slider-news').find('.two').addClass('none');
            }
        });

        this.sliderBuy.forEach(function (slider) {
            if ($(slider.selector).find($('.swiper-slide')).length > 1) {
                new Slider(slider.selector, slider.options)
            } else {
                $(slider.selector).closest('.slider').find('.two').addClass('none');
            }
        });

        this.sliderBuyMobile.forEach(function (slider) {
            if ($(slider.selector).find($('.swiper-slide')).length > 1) {
                new Slider(slider.selector, slider.options)
            } else {
                $(slider.selector).closest('.slider').find('.two').addClass('none');
            }
        });

        // this.galleryThumbs.forEach(function (slider) {
        //     if ($(slider.selector).find($('.swiper-slide')).length > 1) {
        //         new Slider(slider.selector, slider.options)
        //     } else {
        //         $(slider.selector).closest('.slider').find('.two').addClass('none');
        //     }
        // });
        //
        // this.galleryTop.forEach(function (slider) {
        //     if ($(slider.selector).find($('.swiper-slide')).length > 1) {
        //         new Slider(slider.selector, slider.options)
        //     } else {
        //         $(slider.selector).closest('.slider').find('.two').addClass('none');
        //     }
        // });


        if ($('#sliderCard .gallery-top').find($('.swiper-slide')).length > 1) {
            const galleryThumbs = new Swiper('#sliderCard .gallery-thumbs', {
                // direction: 'vertical',
                // loop: 'true',
                spaceBetween: 0,
                slidesPerView: 5,
                speed: 700,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            });
            const card = new Swiper('#sliderCard .gallery-top', {
                slidesPerView: 1,
                speed: 700,
                pagination: {
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-nexttt',
                    prevEl: '.swiper-button-prevvv',
                },
                thumbs: {
                    swiper: galleryThumbs,
                },
                breakpoints: {
                    800:{
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }

    }

    events() {
        let self = this;

        $(window).on('resize', function () {
            // self.media();
        })
    }
}

export class Slider {
    constructor(selector, options) {
        new Swiper(selector, options);
    }
}
