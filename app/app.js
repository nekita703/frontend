
if(window.location.hash) window.setTimeout(() => { window.scrollTo(0, 0); }, 1);

function requireAll(r) {
    r.keys().forEach(r);
}

requireAll(require.context('./icons', true, /\.svg$/));

// Load plugins
import 'jquery'
import IMask from 'imask'

import svg4everybody from 'svg4everybody'
window.svg4everybody = svg4everybody;

import objectFitImages from 'object-fit-images'
window.objectFitImages = objectFitImages;

import imagesLoaded from 'imagesloaded'
window.imagesLoaded = imagesLoaded;

// load modules
import Utils from'./js/utils/utils'
import SvgUse from'./js/svgUse'
import Forms from'./js/forms/forms'
import Modals from'./js/modals'
import MapObject from'./js/map-object'
import Sliders from'./js/sliders'
import Search from'./js/search'
// import MapObject from'./js/map'
// import MapObject from'./js/scrollbar'
// import Video from'./js/video'
import Tabs from './js/tabs'
import ContentShow from './js/content-show'
import changeArrow from './js/changeArrow'
import changeArrowFooter from './js/changeArrowFooter'
import Star from './js/star'
import ValueChecker from './js/value-checker'
import Delete from './js/delete-elems'
import SelectedId from'./js/selected-id'
import Scroll from "./js/scroll-header";
import Debounce from './js/debounce';
import selectCustom from './js/selectCustom';


// import Media from './js/media'

// Load styles
import './styles/app.js';

// Run components

window.App = {
    debug: false,
    lang: 'ru'
};

if (window.SITE_LANG) {
    App.lang = window.SITE_LANG;
}

if (App.debug) {
    console.log('Debug: ' + App.debug);
    console.log('Lang: ' + App.lang);
}

document.addEventListener('DOMContentLoaded', function() {
    objectFitImages();

    if('ontouchstart' in window || navigator.maxTouchPoints) $(document.body).addClass("touch");

    App.Utils = new Utils();
    App.SvgUse = new SvgUse();
    App.Forms = new Forms();
    App.Modals = new Modals();
    App.MapObject = new MapObject();
    App.Sliders = new Sliders();
    App.Search = new Search();
    App.Functions = new ContentShow();
    App.Delete = new Delete();
    // App.Map = new Map();
    // App.Video = new Video();
    // App.Scrollbar = new Scrollbar();
    App.Tabs = new Tabs();
    App.changeArrow = new changeArrow();
    App.changeArrowFooter = new changeArrowFooter();
    App.Star = new Star();
    App.ValueChecker = new ValueChecker();
    App.SelectedId = new SelectedId();
    App.Scroll = new Scroll();
    App.Debounce = new Debounce();
    App.selectCustom = new selectCustom();

    // App.Media = new Media();

    $('[data-inputmask]').each(function () {
        IMask($(this)[0], {mask: "+7 (000) 000-0000"});
    });

    // prevent copying

    $('.no-select').on('selectstart', false);

    $(".no-select img").on('mousedown', false);
});