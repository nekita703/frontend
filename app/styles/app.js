/*Vendors*/
import 'normalize.css';
import 'animate.css'

require("./variables.css");
require("./styles.css");
require("./bx-filter.css");
require("./editor.css");
require("./fancybox.css");
require("./fonts.css");
require("./forms.css");
require("./grid.css");
require("./helpers.css");
require("./icons.css");
require("./lazy-load.css");
require("./media.css");
require("./search.css");

require("./components/main.css");
require("./components/general-catalog.css");
require("./components/original-catalog.css");
require("./components/selected-ts.css");
require("./components/product.css");
require("./components/accessories.css");
require("./components/universal-catalog.css");
require("./components/find-details.css");
require("./components/find-details-end.css");
require("./components/registration-order.css");
require("./order-table.css");
require("./components/confirmation-delivery.css");
require("./ymaps.css");
require("./components/card-ts.css");
require("./components/innovation-chose.css");
require("./components/news-promotions.css");
require("./components/news-block.css");
require("./components/subscription.css");
require("./components/brand-item.css");
require("./components/brands.css");
require("./components/vocabulary.css");
require("./components/brands-blocks.css");
require("./components/brand-presentation.css");
require("./components/page404.css");
require("./components/contacts.css");
require("./components/info-menu.css");
require("./components/contact-cards.css");
require("./components/write-us.css");
require("./components/about-company.css");
require("./components/logistica.css");
require("./components/provider.css");
require("./components/api.css");
require("./components/api-advantages.css");
require("./components/pay-main.css");
require("./components/pay-delivery.css");
require("./components/pay-take.css");
require("./components/pay-transport.css");
require("./components/delivery-table.css");
require("./components/town-delivery.css");
require("./components/text-page.css");
require("./components/custom-select.css");



/*All components*/

function requireAll(requireContext) {
    return requireContext.keys().map(requireContext);
}
const modules = requireAll(require.context("./components", false, /.css$/));
